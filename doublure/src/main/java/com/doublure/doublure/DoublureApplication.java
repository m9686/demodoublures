package com.doublure.doublure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoublureApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoublureApplication.class, args);

    
	}

}
