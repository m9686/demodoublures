package com.doublure.doublure.classes;

import java.util.ArrayList;


public class StubAnimalServer {

  static public Animal getAnimal(int id) {
    Animal animal1 = new Animal("Tiger", 7, true);
    Animal animal2 = new Animal("Cat", 2, true);
    ArrayList<Animal> list = new ArrayList<>();
    list.add(animal1);
    list.add(animal2);
    return list.get(id);
  }
  
}
