package com.doublure.doublure.classes;


public class Animal {

  private String breed;
  private int age;
  private boolean isTamed;
  
  public Animal() {}

  public Animal(String breed, int age, boolean isTamed) {
    this.breed = breed;
    this.age = age;
    this.isTamed = isTamed;
  }

  public String getBreed() {
    return breed;
  }

  public void setBreed(String breed) {
    this.breed = breed;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public boolean isTamed() {
    return isTamed;
  }

  public void setTamed(boolean isTamed) {
    this.isTamed = isTamed;
  }
  
  
  



  



  
}
