package com.doublure.doublure.MethodesToTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import com.doublure.doublure.classes.Animal;
import org.junit.jupiter.api.Test;

public class MethodsToTestTest {

  // With Fake
  @Test
  void testCommentAge() {
    Animal animal = new Animal("Cat", 2, true);
    Animal animal2 = new Animal("Dog", 6, true);
    assertEquals("The Cat is young", MethodsToTest.commentAge(animal));
    assertEquals("The Dog is adult", MethodsToTest.commentAge(animal2));
  }
  // With Stub
  @Test
  void testGetFromServer() {
    Animal animal = MethodsToTest.getFromServer(1);
    assertEquals("Tiger", animal.getBreed());
  }
  // With Dummy
  @Test
  void testSayHello() {
    Animal animal = new Animal();
    assertEquals("Hello Poupi", MethodsToTest.sayHello("Poupi",animal)); 
  }
}
