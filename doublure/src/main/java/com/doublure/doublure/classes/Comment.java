package com.doublure.doublure.classes;

public class Comment {

   static public String commentator(int number) {
    if (number > 0 && number <= 3) {
      return "young";
    } else if (number >= 4 && number <= 12) {
      return "adult";
    } else {
      return "";
    }
  }
  
}
