package com.doublure.doublure.MethodesToTest;

import com.doublure.doublure.classes.Animal;
import com.doublure.doublure.classes.Comment;
import com.doublure.doublure.classes.StubAnimalServer;

public class MethodsToTest {
  


  
  // Le test donne toujours un name, donc lobjet Animal ne sert qu'a remplire la liste de parametres. 
  static public String sayHello(String name, Animal animal) {
    if (name == "")
      return "Hello " + animal.getBreed();
    else {
      return "Hello " + name;
    }
  }
  // La methode Commentator contient des failles evidentes, mais elle peut tout de emem etre utilisee dena le cadre d'uh test...
  static public String commentAge(Animal animal) {
    return "The " + animal.getBreed() + " is " + Comment.commentator(animal.getAge());
  }

  // La methode simule un renvoie d'animal, mais non a partir d'un serveur mais d,une liste en dure...
  static public Animal getFromServer(int id) {
    return StubAnimalServer.getAnimal(id);
  }

}
